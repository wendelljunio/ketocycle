import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    step1: null,
    step2: false,
    step3: null
  },
  mutations: {
    SET_STEP1 (state, payload) {
      state.step1 = payload
    },
    SET_STEP2 (state) {
      state.step2 = true
    },
    SET_STEP3 (state, payload) {
      state.step3 = payload
    }
  },
  actions: {
    setStep1 (context, payload) {
      context.commit('SET_STEP1', payload)
    },
    setStep2 (context) {
      context.commit('SET_STEP2')
    },
    setStep3 (context, payload) {
      context.commit('SET_STEP3', payload)
    }
  },
  modules: {
  }
})
